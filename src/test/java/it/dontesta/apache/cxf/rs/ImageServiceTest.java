package it.dontesta.apache.cxf.rs;

import static org.junit.Assert.assertEquals;
import it.dontesta.apache.cxf.rs.model.Image;

import java.io.InputStream;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.junit.BeforeClass;
import org.junit.Test;

public class ImageServiceTest {
    private static String endpointUrl;

    @BeforeClass
    public static void beforeClass() {
        endpointUrl = System.getProperty("service.url");
    }

    @Test
    public void testGetImageByName() throws Exception {
        WebClient client = WebClient.create(endpointUrl + "/images/get-image-by-name/image_2");
        Response r = client.accept("application/json").get();
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        MappingJsonFactory factory = new MappingJsonFactory();
        JsonParser parser = factory.createJsonParser((InputStream)r.getEntity());
        Image output = parser.readValueAs(Image.class);
        assertEquals("image_2", output.getName());
    }
}
