/*
 * #%L
 * Example a Apache CXF-RS Services
 * %%
 * Copyright (C) 2014 Antonio Musarra's Blog
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * 
 */
package it.dontesta.apache.cxf.ws;

import it.dontesta.apache.cxf.rs.model.Image;
import it.dontesta.apache.cxf.rs.services.IImageService;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.cxf.annotations.Logging;
import org.apache.cxf.annotations.WSDLDocumentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class implements the WS Service Image
 * 
 * @author amusarra
 */
@WebService
@BindingType(value = SOAPBinding.SOAP12HTTP_BINDING)
@WSDLDocumentation(value = "Simple CXF JAX-WS Service via Spring configuration")
@Logging(pretty=true)
public class ImageWSService {
	private final Logger _log = LoggerFactory.getLogger(ImageWSService.class);
	
	@Autowired
	private IImageService imageService;

	/**
	 * Returns an object of type Image specifying the name of the image.
	 * 
	 * @param name
	 * @return
	 * @throws WSImageServiceException
	 */
	@WebResult(name="image")
	@WSDLDocumentation(value = "Returns an object of type Image specifying the name of the image.")
    public Image getImageByName(@WebParam(name="imageName") String name) throws WSImageServiceException {
    	try {
			_log.info("Get image by name : " + name);
			return imageService.getImageByName(name);
		} catch (RuntimeException e) {
			throw new WSImageServiceException(e);
		}
    }

    /**
     * Returns an array of byte of the Image specifying the name of the image.
     * 
     * @param name
     * @return
     * @throws WSImageServiceException
     */
	@WebResult(name="binaryImage")
	@WSDLDocumentation(value = "Returns an array of byte of the Image specifying the name of the image.")
    public byte[] getImageByNameAsStream(@WebParam(name="imageName") String name) throws WSImageServiceException {
    	final byte[] imageAsArrayOfByte;
    	try {
			_log.info("Get image by name as stream : " + name);
			imageAsArrayOfByte = imageService.getImageByNameAsStream(name);
			return imageAsArrayOfByte;
		} catch (RuntimeException e) {
			throw new WSImageServiceException(e);
		}
    }

    /**
     * Returns an object of type Image specifying the imageId of the image.
     * 
     * @param id
     * @return
     * @throws WSImageServiceException
     */
	@WebResult(name="image")
	@WSDLDocumentation(value = "Returns an object of type Image specifying the imageId of the image.")
    public Image getImageById(@WebParam(name="imageId") String id) throws WSImageServiceException {
    	try {
			_log.info("Get image by id : " + id);
			return imageService.getImageById(id);
		} catch (RuntimeException e) {
			throw new WSImageServiceException(e);
		}
    }

    /**
     * Returns an object of type Image specifying the hash of the image.
     * 
     * @param hash
     * @return
     * @throws WSImageServiceException
     */
	@WebResult(name="image")
	@WSDLDocumentation(value = "Returns an object of type Image specifying the hash of the image.")
    public Image getImageByHash(@WebParam(name="imageHash") String hash) throws WSImageServiceException {
    	try {
			_log.info("Get image by hash : " + hash);
			return imageService.getImageByHash(hash);
		} catch (RuntimeException e) {
			throw new WSImageServiceException(e);
		}
    }
}
