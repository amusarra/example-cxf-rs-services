/*
 * #%L
 * Simple CXF JAX-RS webapp service using spring configuration
 * %%
 * Copyright (C) 2014 - 2015 Antonio Musarra's Blog
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package it.dontesta.apache.cxf.ws;

public class WSImageServiceException extends Exception {

	private static final long serialVersionUID = 8254281388127302416L;

	public static final String WS_GENERIC_ERROR_CODE = "WSSEC_0000";
	public static final String WS_GENERIC_ERROR_CODE_DESCRITION = "Errors occurred during the interaction with the service. Check the log files for more details.";
	public static final String WS_GENERIC_ERROR_DESCRITION = "Errors occurred. Check the log files for more details.";

	private String errorCode;
	private String errorDescription;

	/**
	 * 
	 */
	public WSImageServiceException() {
	}

	/**
	 * 
	 * @param message
	 */
	public WSImageServiceException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param message
	 * @param errorCode
	 * @param errorDescription
	 */
	public WSImageServiceException(String message, String errorCode,
			String errorDescription) {
		super(message);
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	/**
	 * 
	 * @param message
	 * @param errorCode
	 * @param errorDescription
	 * @param cause
	 */
	public WSImageServiceException(String message, String errorCode,
			String errorDescription, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public WSImageServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 
	 * @param cause
	 */
	public WSImageServiceException(Throwable cause) {
		super(cause);
		this.errorCode = WS_GENERIC_ERROR_CODE;
		this.errorDescription = WS_GENERIC_ERROR_CODE_DESCRITION;
	}

	/**
	 * Get the custom error code
	 * 
	 * @return
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Get the error description
	 * 
	 * @return
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

}
