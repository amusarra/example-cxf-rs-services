/*
 * #%L
 * Simple CXF JAX-RS webapp service using spring configuration
 * %%
 * Copyright (C) 2014 Antonio Musarra's Blog
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * 
 */
package it.dontesta.apache.cxf.rs.model;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;

/**
 * This class describes objects of type image.
 * 
 * @author amusarra
 *
 */
public class Image {
	private String base64Data;
	private Long contentSize;
	private String contentType;
	private byte[] data;
	private String hash;
	private String id;
	private String name;
	
	/**
	 * @return the base64Data
	 */
	@XmlElement(nillable=false, required=true)
	public String getBase64Data() {
		return base64Data;
	}

	/**
	 * @return the contentSize
	 */
	@XmlElement(nillable=false, required=true)
	public Long getContentSize() {
		return contentSize;
	}

	/**
	 * @return the contentType
	 */
	@XmlElement(nillable=false, required=true)
	public String getContentType() {
		return contentType;
	}

	/**
	 * @return the data
	 */
	@XmlElement(nillable=false, required=true)
	public byte[] getData() {
		return data;
	}
	
	/**
	 * @return the hash
	 */
	@XmlElement(nillable=false, required=true)
	public String getHash() {
		return hash;
	}

	/**
	 * @return the id
	 */
	@XmlElement(nillable=false, required=true)
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	@XmlElement(nillable=false, required=true)
	public String getName() {
		return name;
	}

	/**
	 * @param base64Data the base64Data to set
	 */
	public void setBase64Data(String base64Data) {
		this.base64Data = base64Data;
	}

	/**
	 * @param contentSize the contentSize to set
	 */
	public void setContentSize(Long contentSize) {
		this.contentSize = contentSize;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * @param hash the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Image [base64Data=" + base64Data + ", contentSize="
				+ contentSize + ", contentType=" + contentType + ", data="
				+ Arrays.toString(data) + ", hash=" + hash + ", id=" + id
				+ ", name=" + name + "]";
	}
}
