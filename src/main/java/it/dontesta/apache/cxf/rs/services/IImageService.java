/*
 * #%L
 * Example a Apache CXF-RS Services
 * %%
 * Copyright (C) 2014 Antonio Musarra's Blog
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * 
 */
package it.dontesta.apache.cxf.rs.services;

import it.dontesta.apache.cxf.rs.model.Image;

/**
 * @author amusarra
 *
 */
public interface IImageService {
	
	/**
	 * Returns an object of type Image specifying the name of the image.
	 * 
	 * @param imageName The name of the image
	 * @return {@link Image} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
	public Image getImageByName(String imageName) throws RuntimeException;
	

	/**
	 * Returns an array of byte of the Image specifying the name of the image.
	 * 
	 * @param imageName The name of the image
	 * @return {@link Image} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
	public byte[] getImageByNameAsStream(String imageName) throws RuntimeException;

	/**
	 * Returns an object of type Image specifying the imageId of the image.
	 * 
	 * @param imageId The image Id of the image
	 * @return {@link Image} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
	public Image getImageById(String imageId) throws RuntimeException;

	/**
	 * Returns an object of type Image specifying the hash of the image.
	 * 
	 * @param hash The hash (md5) of the image
	 * @return {@link Image} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
	public Image getImageByHash(String hash) throws RuntimeException;
}
