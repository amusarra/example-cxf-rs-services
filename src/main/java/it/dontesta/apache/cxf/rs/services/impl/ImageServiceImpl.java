/*
 * #%L
 * Example a Apache CXF-RS Services
 * %%
 * Copyright (C) 2014 Antonio Musarra's Blog
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * 
 */
package it.dontesta.apache.cxf.rs.services.impl;

import it.dontesta.apache.cxf.rs.model.Image;
import it.dontesta.apache.cxf.rs.services.IImageService;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author amusarra
 *
 */
public class ImageServiceImpl implements IImageService {

	/**
	 * Get bytes from InputStream
	 * 
	 * @param is
	 * @return A array of bytes of the resources 
	 * @throws IOException
	 */
	private static byte[] _getBytes(InputStream is) throws IOException {
	    int len;
	    int size = 1024;
	    byte[] buffer;

	    if (is instanceof ByteArrayInputStream) {
	      size = is.available();
	      buffer = new byte[size];
	      len = is.read(buffer, 0, size);
	    } else {
	      ByteArrayOutputStream bos = new ByteArrayOutputStream();
	      buffer = new byte[size];
	      while ((len = is.read(buffer, 0, size)) != -1)
	        bos.write(buffer, 0, len);
	      buffer = bos.toByteArray();
	    }
	    return buffer;
	  }
	
	/**
	 * Get a size of a file (in bytes).
	 * 
	 * @param resourceURL
	 * @return The size of the resources
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private static long _getContentSize(URL resourceURL) throws IOException, URISyntaxException {
		return Files.size(Paths.get(resourceURL.toURI()));
	}

	/**
	 * Get a content type of the resource.
	 * The content type is compliant with MIME RFC 2045
	 * 
	 * @param resourceURL
	 * @return The contentType of resources
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	private static String _getContentType(URL resourceURL) throws IOException, URISyntaxException {
		return Files.probeContentType(Paths.get(resourceURL.toURI()));
	}

	/**
	 * Return the hash of the stream
	 * 
	 * @param resourceURL
	 * @return MD5 hash of the resources
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws URISyntaxException 
	 */
	private static String _getHash(URL resourceURL) throws IOException, URISyntaxException {
		File imageFile = new File(resourceURL.toURI());
		InputStream is = new FileInputStream(imageFile);
		
		return DigestUtils.md5Hex(is);
	}

	/**
	 * Return image for resource not found
	 * 
	 * @return {@link Image} Object
	 */
	private static Image _getResourceNotFound() {
        Image image = new Image();
        image.setId(new BigInteger(64, new SecureRandom()).toString(32));
        image.setName("Resource Not Found");
        image.setBase64Data("data:image/png;base64,"
        		+ "iVBORw0KGgoAAAANSUhEUgAAAOEAAAAvCAMAAAAij12MAAAAA3NCSVQICAjb4U/gAAABgFBMVEUzMzPq6+i4u7Zra2vMAACkAADKamrTISHU1tG1tbWCMjOZmZk6Ojr9+PjbSkrNSkrg4t/Iy8W+MTHWjIyMjIxMUFF9GhvfpqbXOTnZ29f39/eprafCxb+JPj/dU1PRfn7AT097e3uaEBHnvb2zGhpDQ0NTV1jZlJTWMTHSWVnPDw+lpaWsFxe1MDDe3t7ha2vZQkK9RUXlfHzcnp7m5ubrxsaDg4Pira3039+UlJSmBweyKCitEBCFQUJLS0tmZmbVKSnMzMwuNDbv7+9ycnKcHB3HYmL25eX////WYGDecnJSNTfmjIy4ODifLS7fenrw1tbnhIT68PDnk5Puq6tSUlK9vb3OCAjW1ta+SEjNUlLeQkKtra3Nc3PsnZ20ICDR087EOTntpaXlt7evICC5Tk7EWlrc3dlCOTvj5OHFxcW/wr3WbGzjc3O8KChaWlqqCwvLzceMFRbThYWDODmeJifRGRnhhITuzs6vs65SUlreWlrr7OrqxMTfYmLv9+9pUqGeAAAACXBIWXMAAAsSAAALEgHS3X78AAAAGHRFWHRDcmVhdGlvbiBUaW1lADIwLTA0LTIwMTTwK+3kAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M1cbXjNgAACzlJREFUaIHVWgtX20YWHgsmWLUNduO2MXZpvAVCayIc1etGDktQxSYu62wCjhU1x3EEycaRHB7ddjf1wZvfvt+dkWQDhs2mDw7fOQhpZu7jmztzZ0YWq5/E9rdXn7168OD6lZ++uX2q8hKCHX+8f+NWjQ9x84tfLsat3xDHGN5+BVa1/Zmn329uvvtqZucQjw++vCjXfiOMMNy+UuO1ne+3thoRNmeWOf/hcsdxyPDFKuc7eqOhH5WX1qem1pfKR3qz2fwKxT9doIO/GhHDq5x//LDROFqaCrG+vn7tqNFsPq3xV6mL9PHXIWCYusf500ZjZSmgJrEEvGs2H67ym/cv1s1fgYDhPX74prlVHqVGuAaUtzqN5/zO9sX6+eGQDK/yQ72pLw1xLUK5XN7rdPb5Dxfs6AdDMHzB+WZz7zS1ECu7zWX+7KJd/UAQw/uH/KuOfpradAR9t1Hj3160rx8GYniP73QaIyEboTYXoPF4k7+8nAkVDB/xWqMzdwa1uSOJzuN9fuOinf0ggOF1/nRXP4Na9LSy+5YvX8p8yuq/8NVOZ+5MaiGauzv8qhAxTTOT8WcF/siB208YaTL5PjZN0wpvWf0LCuE51OZkbN/tPuQPpIhE1bOsP5JjFjaN9+1WxjKpoCWr3+RvH787h1qIZueQ35fSRcNYY2yiEiOOvyfFRMKrp4KgLbBSN2e9Z68yFg0ydpuvPu6Azj/+ciY+//xzse7v8G+kdGFQ8e6im7zK4PelCBtR1GDWGsRiA+L4PgytoDfYDT7zmAbp64XFEK1W65MIa4TvQPGo84Zfh3CKMbui5UqMtXOaF/v/RqoMyJlTKXWsJpWiUAg/Z1OzZNZLp8/u1VQkLRrHKhVyLsWe8Te774hhW1TK2BL/AaECeJ6W/BlB7DT4nYBh2nHBsGQ6uXQ7a7SFjG8YJU3c2YYsGmQyMGrRFZe0YSgojKNSNFOCVsJby3SskmFI+VSgQPMZS9iaCJu4V30n181ns/lT2p3BULptGC5jSlqT/c9e8Ye7R5hmr9upVMQOiAEgB2haLvkz9jrNJl+lPoB0181MYNT4ZqZFSWexYlkTIv3kLauyGBYVGCPmdMUlydhdy5PNRlsJjiZjC3hOmtZIlSHaqgibFdxXXV8a9MZoH5WGWM5xNA/xZrf4211KJK9LQ3oDYhfS63a7jmC4hVSTEgxVxywi09i2f8CSqtpixUqWJftaKZkdDO6yRdNZZHdjMZuokCeW1YPNkuvHqixZ0rKMWiVte5EVYyJbzWaodyA1gaEVKXAVZM++iblQsXJQY7SVzAFrKejWgzHaW4F0sudsYAq5pptLV8DwDm92pjEGwfDE2AQ7Qc9x3ORHYKh35vltwbB1gC6bUFUlAU0OTUntgBXRPGN66E+3UkFMVE9lDIrgyYDcyXppDcV50XHwXfG8PATTYihByoiJZ60QKdC60lGnizYWY33XRwsl7vjjtGuhtFLxNDDO+GZcI4bzvNksC4YnxmaX4MTjcdcFQxw79jrP+SPBkLCQVVV1gy2USnmozqNvbSfuOLkqa4FqDoxzbTKpwRMvjYuPYVNlCzmg282ytX6/3RYdhI624LPmdfFMbaCgSwocVzjquzmtMhigrekX2ZpvuvHT2t1cPB9J5xwI+gUIeoNZ9oC/7QiGicHo1OuSJ5Ke6/4oGOo4Qm0Lhn03j+lQ6vdbwerPSogmm0hkTJM8c+JulR2YJTLpwJMcueOaotKEvrhzEAliLCFCGGdeNw6BjFTgCgUYu4meXTAxoWIxYthbZBsF3zfHac+MSJuUoiAoGF7nDztljMHXiZGxeYwekPwIB/+tZq1WDxgW2kk20QbDuwkB1U7QDF/I+0QC27oiOyCTuMU1TpkXwaBKdAJIHrCJfKkEQcWH/5UBxlk6R019fzFS4ItcqtgZtxswLKDSBsVx2nuJSDrjRwxjs+wKf9OZJoaGNzr1nJBdBigIho0GfxkxVCdYq91usQ3bVhRF/LWrSZYkE9i1kg++MOmTJ+SO36PUsuH74AiGVTNTgPd2IeNqFZpJmpOBgEg/kBIKUJpQVNvHfIoNIoZ2b6x2ZShd6AmGvmSIbWnnSDBMp9OngvejXyAkP/331FJTbkwDhtgm9vugqagB4C1sJDbYYqHQE8NJmLSz5Im4VWzMW58ounDR9G0S6vmIECWMritaKRNssdezhQJiqKp2xpHzEJkGlVA5VrtqSOk19KQqGcp5+ILvd+gNzesq8QF6AEJCnveBPCH56eTkta0Z8SZDMPQVgyaiQS70+4mWWiohkOjLBEzmbTtPJmC4r2AtYQW6pTBTF4gg0o6ooKiJlmKfZCgUKEIBrRYhQ5lLDaG9PU67ZEhu2UoxYogYpg55owmGf/vuPPxzcvJIn+f/iRgG1lssaeSx+igbyaJaWmMLirrG1kqJNQTCVpLsbhuV0jAxLGD9TNhFRjknmeijDqPUkaM0ZEgK8qSgQDaKRnvIMO7bYeVp7ZKhkDaSozGsv+LfN6cxzf5O+Ndf/zQWk5NP9E2+XK8HKz7m0AIrKkp/jRJismiTUmSakkI8gTUVDlZFZegDTSD7E1FkmnJvkixiX4TlYiDmoZmggCh5qUAp9GzKXome6dCCIjYafj+sPKEdmUBKl5KyNCHHPzH8jO83dfGWdOrrJ5NnoqzP8HuCYc92ckhX+VK/gIlgbGxUVaQ326gWsz5MqGp1YyOLjIfaRLFYbScw3NoGLIKNaaLI8F1X3GQhaJIblmYrWs60SyUosgtZtIFKTNRqsaqKhGFZNsy6yGFUibqT2jH5pLRSLRbzyO69Qibe9bDi17eX+ZvG9NS59CYnv9b1QzFI67NWzNMcWt1MWgDIlC8TpChAZ8IapXQkKVFLUEQBticyRwuYJG2KxQJbjQrWw0inEKV/dBGLBe1GYHbY4rR2P6rDGgqIdYgY1m/weX3lPHaEaX1HnJ3o9GHBG0euKFKpXOMcSbEnncvgPkMFZAwmyb5LqXpI0sxQv4g9DVFMowp7BVOyIFmqN02x4NOGsuLlUC8bjNGO/gukZRfQ+I8Jhqk7/Kk+fT7BKX2zRls2cfgiW5omdgZE05TbFCcnKUuvRCGKiUZGdALil9PSadpOOI4TLrnBAUBQFBtFUQmxoBewctG+biD2zJV0l6TjYRcd146TlZQO+kCODmKImXi4qS+dO0b39H05C4nhLLkjd0BpuQGCZ5om78mSvKBYdgJ55NCDJ4TSonPk1qkbHOJI50DuGLEsY2cpQWq7WlocemdngxbBnuuUdi2U7gZdSIMbJ2By+jrGqf7vswk+WcEYfRm+SxyeIQd0yJJ+eV5wBxPikg7LBCHJTkYiOJgFgvJlT8ARiA1PblIBTkSD0RaxYxZHtKMdORQLS+WpRTLcvsn3dX39zAiu6E/54aPh24LZENFpMhYbhO7Fwv+xiFAlZBfIiLZScDCQZ+DRw7doEQsqrVBu1OJgrPahdCzqmFTw29PtZf6xrpfPmIN7IHjsV4sRjscdC/0Z9WtYelxipBW9Xxmrc/iMJqlhk/O1j2qOfj/88yF/vqcffT1mhJZ1DNHgZfDx1z4Ss+dhtDZ8C3VKIvU/daaOmT1L+ynplHxfKvHoJV99p+vTJzle29P39nntszHvti4Jot/xt29x/vGmrh9di0g+WZ/b0/WZGp+/zF9jDL/FSP10yPkOOOorR3Pl8vQRQgp+q5y/urw/4tePfzF0/0qN8/kdwYywOfOcc37rMgewfvKrr0f3luljr8P5/f35VbqrXb/sn0Sd/K4NWfXZzVX5UVvt5b1vL+fPvsdwiiFh+9GXL365lD+HjsF/Ab18cBD1pR8RAAAAAElFTkSuQmCC");
        
		return image;
	}

	/**
	  * Encodes the byte array into base64 string
	  *
	  * @param imageByteArray - byte array
	  * @return String a {@link java.lang.String}
	  */
	private static String encodeImage(byte[] imageByteArray) {
		return Base64.encodeBase64String(imageByteArray);
	}

	
	private final Logger _log = LoggerFactory.getLogger(ImageServiceImpl.class);
	
	/**
	 * Return the image object by name
	 * 
	 * @param imageName
	 * @return {@link Image} Object
	 */
	private Image _getImageByName(String imageName) {
		String resource = "/images/my/" + imageName + ".png";
		InputStream imageIs = ImageServiceImpl.class.getResourceAsStream(resource);
		
		if(imageIs != null) {
	        Image imageByName = null;
	        byte[] imageAsArray = null;
	        try {
	        	imageAsArray = _getBytes(imageIs);
		        String imageDataString = encodeImage(imageAsArray);
		        String contentType = _getContentType(ImageServiceImpl.class.getResource(resource));
		        Long contentSize = _getContentSize(ImageServiceImpl.class.getResource(resource));
		        
		        imageByName = new Image();
				imageByName.setId(new BigInteger(64, new SecureRandom()).toString(32));
				imageByName.setName(imageName);
				imageByName.setContentType(contentType);
				imageByName.setContentSize(contentSize);
				imageByName.setBase64Data("data:" + contentType + ";base64," + imageDataString);
				imageByName.setHash(_getHash(ImageServiceImpl.class.getResource(resource)));	
				imageByName.setData(imageAsArray);
			} catch (IOException | URISyntaxException ioeUse) {
				_log.warn("Error on reading image resources => "
						+ ioeUse.getMessage());
				imageByName = _getResourceNotFound();
			} finally {
				try {
					imageIs.close();
				} catch (IOException ioe) {
					_log.warn("Error on closing resources => " 
							+ ioe.getMessage());
				}
			}
	        return imageByName;
		} else {
	        return _getResourceNotFound();
			
		}
	}
	
	@Override
	public Image getImageByHash(String hash) throws RuntimeException {
		throw new RuntimeException("Method is not implemented.");
	}

	@Override
	public Image getImageById(String imageId) throws RuntimeException {
		throw new RuntimeException("Method is not implemented.");
	}


	@Override
	public Image getImageByName(String imageName) throws RuntimeException {
		_log.info("Call the it.dontesta.apache.cxf.rs.services.ImageServiceImpl.getImageByName(String)...");
		return _getImageByName(imageName);
	}

	@Override
	public byte[] getImageByNameAsStream(String imageName)
			throws RuntimeException {
		String resource = "/images/my/" + imageName + ".png";
		byte[] resourceAsArrayOfByte = null;
		
		try {
			URL resourceURL = ImageServiceImpl.class.getResource(resource);
			if(resourceURL != null) {
				resourceAsArrayOfByte = Files.readAllBytes(Paths.get(resourceURL.toURI()));
				return resourceAsArrayOfByte;
			} else {
				throw new RuntimeException("Resource " 
						+ resource 
						+ " not found");
			}
		} catch (IOException ioe) {
			_log.warn("Error on read resources as byte => " 
					+ ioe.getMessage());
			throw new RuntimeException("Error on read resources as byte => " 
					+ ioe.getMessage());
		} catch (URISyntaxException ioeUse) {
			_log.warn("Error on URI resources => " 
					+ ioeUse.getMessage());
			throw new RuntimeException("Error on URI resources => " 
					+ ioeUse.getMessage());
		}
	}
}
