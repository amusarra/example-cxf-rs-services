/*
 * #%L
 * Example a Apache CXF-RS Services
 * %%
 * Copyright (C) 2014 Antonio Musarra's Blog
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * 
 */
package it.dontesta.apache.cxf.rs;

import it.dontesta.apache.cxf.rs.services.IImageService;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class implements the REST service Image Rest Service
 * 
 * @author amusarra
 */
@Path("/images")
public class ImageRestService {
	private final Logger _log = LoggerFactory.getLogger(ImageRestService.class);
	
	@Autowired
	private IImageService imageService;

	/**
	 * Returns an object of type Image specifying the name of the image.
	 * <p>Example a returned JSON </p>
	 * <pre>
	 * {
	 *	    "base64Data": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAAAvCAMAAAAij1SuQmCC....",
	 *	    "contentSize": 164478,
	 *	    "hash": "9b1dbd0754cd49e8eb65f67d8252c2be",
	 *	    "data": "iVBORw0KGgoAAAANSUhEUgAAAOEAAAAvCAMAAAAij1SuQmCC....",
	 *	    "contentType": "image/png",
	 *	    "name": "image_4",
	 *	    "id": "4q543ersmgvi4"
	 *	}
	 * </pre>
	 * @param name The name of the image
	 * @return {@link Response} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
    @GET
    @Path("/get-image-by-name/{name}")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Response getImageByName(@PathParam("name") String name) {
    	try {
			_log.info("Get image by name : " + name);
			return Response.ok(imageService.getImageByName(name)).build();
		} catch (RuntimeException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new String(e.getMessage())).build();
		}
    }

	/**
	 * Returns an array of byte of the Image specifying the name of the image.
	 * 
	 * @param name The name of the image
	 * @return {@link Response} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
   @GET
    @Path("/get-image-by-name-as-stream/{name}")
    public Response getImageByNameAsStream(@PathParam("name") String name) {
    	final byte[] imageAsArrayOfByte;
    	try {
			_log.info("Get image by name as stream : " + name);
			imageAsArrayOfByte = imageService.getImageByNameAsStream(name);
			return Response.ok(new StreamingOutput() {
				
				@Override
				public void write(OutputStream os) throws IOException,
						WebApplicationException {
					os.write(imageAsArrayOfByte);
				}
			}).build();
		} catch (RuntimeException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new String(e.getMessage())).build();
		}
    }

	/**
	 * Returns an object of type Image specifying the imageId of the image.
	 * 
	 * @param id The image Id of the image
	 * @return {@link Response} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
    @GET
    @Path("/get-image-by-id/{id}")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Response getImageById(@PathParam("id") String id) {
    	try {
			_log.info("Get image by id : " + id);
			return Response.ok(imageService.getImageById(id)).build();
		} catch (RuntimeException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new String(e.getMessage())).build();
		}
    }

	/**
	 * Returns an object of type Image specifying the hash of the image.
	 * 
	 * @param hash The hash (md5) of the image
	 * @return {@link Response} Object
	 * @throws RuntimeException In case of errors during the reading of the resource.
	 */
    @GET
    @Path("/get-image-by-hash/{hash}")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    public Response getImageByHash(@PathParam("hash") String hash) {
    	try {
			_log.info("Get image by hash : " + hash);
			return Response.ok(imageService.getImageByHash(hash)).build();
		} catch (RuntimeException e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new String(e.getMessage())).build();
		}
    }
}
